import React from 'react';
import { View, Text, Button, Alert } from 'react-native';

const StatefulComponent = () => {
	const uneVariableAuPifPourLexemple = 'ça marche';
	const showAlert = () => {
		Alert.alert('Bravo !', `On a défini qu'à l'appui du bouton, on affichait un message d'alerte.

Dans ce callback, je peux très bien accéder aux variables déclarées dans mon composant, ou aux props du composant,
car tout ceci est du JS classique.

\`uneVariableAuPifPourLexemple = "${uneVariableAuPifPourLexemple}"\``)
	}

	return (
		<View>
			<Text style={{marginBottom: 12}}>
				Appuyez sur le bouton
			</Text>
			<Button
				onPress={showAlert}
				title="Allez, appuyez, quoi"
			/>
		</View>
	);
}

export default StatefulComponent
