import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import Paragraph from "../../components/Paragraph";
import Bold from "../../components/Bold";
import Heading from "../../components/Heading";

const HomemadeComponents = (props) => {
	return (
		<View>
			<Heading lvl="1">{props.children}</Heading>

			<Paragraph>
				C'est <Bold>paragraphe</Bold>. Il peut passer sur plusieurs lignes tout seul si jamais il a besoin, évidemment.
			</Paragraph>

			<Paragraph>
				Ceci est un autre paragraphe, <Bold>plus court</Bold>.
			</Paragraph>

			<Heading lvl="2">
				Exemples d'expressions JS (voir le code)
			</Heading>

			<Paragraph>
				Vous saviez que 2 + 2 = { 2 + 2 } ?
			</Paragraph>

			<Paragraph>
				Mais aussi que 2 + 2 = {
				"je teste quelque chose" === "de faux"
					? 5
					: 4
				} ?
			</Paragraph>

			<Paragraph>
				Et aussi que 2 + 2 = { "ceci est truethy, retourne donc ce qui est après le &&" && 4 } ?!
			</Paragraph>

			<Heading lvl="2">
				Date du prochain cours
			</Heading>

			<View style={{alignItems: 'center'}}>
				<Paragraph>Rendez-vous vendredi chez : </Paragraph>
				<Image
					source={require('../../assets/logo-ca.png')}
					style={{width: 200, height: 200}}
					accessibilityLabel="Campus Academy"
				/>
			</View>
		</View>
	);
}

export default HomemadeComponents
