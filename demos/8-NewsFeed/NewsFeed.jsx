// import React from 'react';
// import {  } from 'react-native';

import { useState, useEffect } from "react";
import { View, FlatList, Text, Image, TouchableOpacity } from "react-native";
import * as Linking from "expo-linking"

/**
 * Exercice : créer une vue qui va afficher une liste des dernières news du site MyDigitalSchool.
 *
 * La liste de news est disponible au format JSON sur https://mds-api.netlify.app/news.json.
 *
 * Réfléchir à comment récupérer ces données,
 * puis comment, pour chacune des news :
 *
 * - afficher son titre
 * - afficher son extrait texte (champ "excerpt")
 * - permettre à l'utilisateur de cliquer sur le titre de la news pour ouvrir le navigateur web de son téléphone sur la page de la news (champ "url").
 * 		- voir le composant TouchableOpacity pour avoir un bouton "invisible" en tant que titre,
 * 		- voir "expo-linking" (l'installer) pour ouvrir le navigateur : https://docs.expo.io/versions/v41.0.0/sdk/linking/.
 *
 * Prendre le temps de faire une liste avec un design un minimum compréhensible (par exemple, titres écrits plus gros, en gras, éléments de la liste bien séparés, etc. Faites comme vous voulez)
 *
 * Utiliser le composant FlatList pour afficher la liste de news : https://reactnative.dev/docs/flatlist
 *
 * Si vous avez fini tout ça, améliorez la liste pour afficher, pour chaque news, son image à côté du titre et du texte.
 */
const MDSNews = () => {

	const [news, setNews] = useState([])
	useEffect(() => {
		fetch('https://mds-api.netlify.app/news.json')
			.then(res => res.json())
			.then(setNews)
	}, [])

	if (!news.length) {
		return <Text>Chargement…</Text>
	}

	const onItemPress = (item) => {
		Linking.openURL(item.url)
	}
	
	return (
		<FlatList
			data={news}
			renderItem={({ item }) => (
				<View style={{
					flexDirection: "row",
					marginBottom: 20
				}}>
					<Image
						source={{ uri: item.image }}
						style={{
							width: 100,
							height: 100
						}}
						resizeMode="contain"
					/>
					<View style={{ flex: 1, marginLeft: 10 }}>
						<TouchableOpacity onPress={() => onItemPress(item)}>
							<Text style={{ fontWeight: "bold", fontSize: 24 }}>
								{item.title}
							</Text>
						</TouchableOpacity>

						<Text style={{ fontSize: 20 }}>{item.excerpt}</Text>
					</View>
				</View>
			)}
		/>
	)
}


export default MDSNews
