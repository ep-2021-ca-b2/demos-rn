import React from "react";
import { Text, StyleSheet } from "react-native";

const lvlMapping = {
	"1": "header",
	"2": "subheader"
}

const Heading = ({ lvl, children }) => {
	return (
		<Text
			style={styles[lvlMapping[lvl]]}
		>
			{children}
		</Text>
	)
}

export default Heading;


const styles = StyleSheet.create({
	header: {
		fontWeight: 'bold',
		fontSize: 24,
		marginBottom: 12
	},
	subheader: {
		fontWeight: 'bold',
		fontSize: 20,
		marginVertical: 12
	},
})