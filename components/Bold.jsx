import React from "react";
import { Text } from "react-native";

const Bold = ({ children }) => {
	return (
		<Text style={{
			fontWeight: 'bold'
		}}>
			{children}
		</Text>
	)
}

export default Bold;
