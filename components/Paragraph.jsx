import React from 'react';
import { Text, StyleSheet } from "react-native";

export default function Paragraph({ children }) {
	return (
		<Text style={styles.text}>
			{children}
		</Text>
	)
}

const styles = StyleSheet.create({
	text: {
		fontSize: 16,
		lineHeight: 24,
		marginBottom: 12
	}
})

